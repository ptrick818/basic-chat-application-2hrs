import React, { useState, useEffect } from 'react';
import Login from './Components/Login';

const App = ({ socket }) => {
  const [isLogged, setIsLogged] = useState(false);
  const [username, setUsername] = useState('');
  const [messages, setMessages] = useState([]);
  const [inputValue, setInputValue] = useState('');
  const [contacts, setContacts] = useState(['alice', 'bob']);
  const [selectedRecipient, setSelectedRecipient] = useState(null);

  useEffect(() => {
    socket.on('message', (data) => {
      setMessages((prevMessages) => [...prevMessages, data]);
    });

    return () => {
      socket.off('message');
    };
  }, [socket]);

  const handleLogin = (username) => {
    setUsername(username);
    setIsLogged(true);
    socket.emit('registerUser', username);
  };

  const sendMessage = () => {
    if (inputValue.trim() !== '' && selectedRecipient) {
      const message = { sender: username, recipient: selectedRecipient, text: inputValue };
      socket.emit('message', message);
      setMessages((prevMessages) => [...prevMessages, message]); // add message to local state as well
      setInputValue('');
    }
  };

  if (!isLogged) {
    return <Login onLogin={handleLogin} />;
  }

  return (
    <div className="App">
      <h1>Chat Application</h1>
      <div className="contacts-container">
        <h2>Contacts</h2>
        {contacts.map((contact, index) => (
          <div key={index} className="contact">
            {contact}
            <button onClick={() => setSelectedRecipient(contact)}>Select</button>
          </div>
        ))}
      </div>

      {selectedRecipient && (
        <div className="chat-box">
          <h3>Chat with {selectedRecipient}</h3>
          <div className="message-container">
            {messages
              .filter((message) => message.sender === selectedRecipient || message.sender === username)
              .map((message, index) => (
                <div key={index} className="message">
                  <strong>{message.sender}: </strong> {message.text}
                </div>
              ))}
          </div>
          <div className="input-container">
            <input
              type="text"
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
            />
            <button onClick={sendMessage}>Send</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default App;
