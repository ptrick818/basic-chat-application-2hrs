const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const PORT = process.env.PORT || 5000;

const users = {};


io.on('connection', (socket) => {
  console.log('New client connected');


  socket.on('registerUser', (username) => {
    users[username] = socket.id;
  });


  socket.on('message', (data) => {
    const { sender, recipient, text } = data;
    const recipientSocketId = users[recipient];
    if (recipientSocketId) {
      io.to(recipientSocketId).emit('message', { sender, text });
      io.to(users[sender]).emit('message', { sender, text });
    }
  });


  socket.on('disconnect', () => {
    for (let username in users) {
      if (users[username] === socket.id) {
        delete users[username];
        break;
      }
    }
    console.log('Client disconnected');
  });
});

server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
